﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MB_Marketing.Startup))]
namespace MB_Marketing
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
